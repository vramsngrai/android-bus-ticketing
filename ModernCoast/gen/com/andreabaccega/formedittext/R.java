/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.andreabaccega.formedittext;

public final class R {
	public static final class attr {
		public static final int classType = 0x7f010070;
		public static final int customFormat = 0x7f01006e;
		public static final int customRegexp = 0x7f01006d;
		public static final int emptyAllowed = 0x7f01006f;
		public static final int emptyErrorString = 0x7f01006c;
		public static final int testErrorString = 0x7f01006b;
		public static final int testType = 0x7f01006a;
	}
	public static final class id {
		public static final int alpha = 0x7f05001c;
		public static final int alphaNumeric = 0x7f05001d;
		public static final int creditCard = 0x7f05001f;
		public static final int custom = 0x7f050025;
		public static final int date = 0x7f050028;
		public static final int domainName = 0x7f050021;
		public static final int email = 0x7f05001e;
		public static final int ipAddress = 0x7f050022;
		public static final int nocheck = 0x7f050024;
		public static final int numeric = 0x7f05001b;
		public static final int personFullName = 0x7f050027;
		public static final int personName = 0x7f050026;
		public static final int phone = 0x7f050020;
		public static final int regexp = 0x7f05001a;
		public static final int webUrl = 0x7f050023;
	}
	public static final class string {
		public static final int error_creditcard_number_not_valid = 0x7f0a0012;
		public static final int error_date_not_valid = 0x7f0a0019;
		public static final int error_domain_not_valid = 0x7f0a0014;
		public static final int error_email_address_not_valid = 0x7f0a0011;
		public static final int error_field_must_not_be_empty = 0x7f0a0010;
		public static final int error_ip_not_valid = 0x7f0a0015;
		public static final int error_notvalid_personfullname = 0x7f0a0018;
		public static final int error_notvalid_personname = 0x7f0a0017;
		public static final int error_only_numeric_digits_allowed = 0x7f0a000d;
		public static final int error_only_standard_letters_are_allowed = 0x7f0a000f;
		public static final int error_phone_not_valid = 0x7f0a0013;
		public static final int error_this_field_cannot_contain_special_character = 0x7f0a000e;
		public static final int error_url_not_valid = 0x7f0a0016;
	}
	public static final class styleable {
		public static final int[] FormEditText = { 0x7f01006a, 0x7f01006b, 0x7f01006c, 0x7f01006d, 0x7f01006e, 0x7f01006f, 0x7f010070 };
		public static final int FormEditText_classType = 6;
		public static final int FormEditText_customFormat = 4;
		public static final int FormEditText_customRegexp = 3;
		public static final int FormEditText_emptyAllowed = 5;
		public static final int FormEditText_emptyErrorString = 2;
		public static final int FormEditText_testErrorString = 1;
		public static final int FormEditText_testType = 0;
	}
}
