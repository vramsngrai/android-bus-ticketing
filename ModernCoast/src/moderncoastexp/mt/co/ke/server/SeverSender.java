package moderncoastexp.mt.co.ke.server;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import android.util.Log;

public class SeverSender {
	
	public SeverSender(){
		
	}
	
	public String POSTsignup(String url, GlobalData person){
		
		
        InputStream inputStream = null;
        String result = "";
        try {
 
            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();
 
            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);
 
            String json = "";
 
           
           // Log.d("ded1"," >>>>we here");
            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("fname", person.getUserFname());
            jsonObject.accumulate("lname", person.getUserLname());
            jsonObject.accumulate("email", person.getUserEmail());
            jsonObject.accumulate("phone", person.getUserphone());
            jsonObject.accumulate("command", myconstants.sigupcommand);
           
 
            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();
          
 
            // ** Alternative way to convert Person object to JSON string usin Jackson Lib 
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person); 
 
            // 5. set json to StringEntity
            StringEntity se = new StringEntity(json);
 
            // 6. set httpPost Entity
            httpPost.setEntity(se);
            
        
 
            // 7. Set some headers to inform server about the type of the content   
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
          
           
            
 
            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);
 
            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
 
            // 10. convert inputstream to string
            if(inputStream != null){
                result = convertInputStreamToString(inputStream);
                JSONObject jObject = new JSONObject(result);
                person.setrescode(jObject.getString("result"));
            }
            else{
                result = "9999";
            }
 
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        
        Log.d("result", result);
     //   Log.i("ded"," >>>> "+result);
        // 11. return result
        return result;
        //return "we here";
    }
	
	
public String GetVlist(String url, GlobalData person){
		
		
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
 
            HttpPost httpPost = new HttpPost(url);
 
            String json = "";
 
            JSONObject jsonObject = new JSONObject();
           
            jsonObject.accumulate("phone", person.getUserphone());
            jsonObject.accumulate("command", myconstants.vlistcommand);
 
            
            json = jsonObject.toString();
          
 
 
            StringEntity se = new StringEntity(json);
 
    
            httpPost.setEntity(se);
            
        
 
           
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
          
           
            
 
         
            HttpResponse httpResponse = httpclient.execute(httpPost);
 
        
            inputStream = httpResponse.getEntity().getContent();
 
        
            if(inputStream != null){
                result = convertInputStreamToString(inputStream);
                JSONObject jObject = new JSONObject(result);
                
                person.setrescode(jObject.getString("result"));
                
                //JSONArray list=  jObject.getJSONArray("vlist");
                
                person.setliststore(jObject.getJSONArray("vlist"));
                Log.d("ded1"," >>>>we here "+jObject.toString());
                
            }
            else{
                result = "9999";
            }
 
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        
        Log.d("result", result);
     //   Log.i("ded"," >>>> "+result);
        // 11. return result
        return result;
        //return "we here";
    }
	

public String PostTicket(String url, GlobalData person){
	
	
    InputStream inputStream = null;
    String result = "";
    try {

        HttpClient httpclient = new DefaultHttpClient();

        HttpPost httpPost = new HttpPost(url);

        String json = "";

        JSONObject jsonObject = new JSONObject();
 
       
        jsonObject.accumulate("mobileno", person.getUserphone());
        jsonObject.accumulate("seatno", person.getseatno());
        jsonObject.accumulate("schedule_id",person.getscheduleid());
        jsonObject.accumulate("stationfrom", person.getfrom());
        jsonObject.accumulate("stationto", person.getto());
        jsonObject.accumulate("required_amount", person.getamount());
        jsonObject.accumulate("cardno", person.getcardno());
        jsonObject.accumulate("voucerno", person.getvouchernumbr());
        jsonObject.accumulate("clientname", person.getUserFname()+" "+person.getUserLname());
        jsonObject.accumulate("ref_no", person.getmpesaref());
        jsonObject.accumulate("currency", "KES");
        jsonObject.accumulate("bookingmode", 4);
        jsonObject.accumulate("channel", 4);
        jsonObject.accumulate("userid", 0);
        jsonObject.accumulate("nationality", "Kenyan");

        
        json = jsonObject.toString();
      
        System.out.println("Theresquest >>>>> "+ json);

        StringEntity se = new StringEntity(json);


        httpPost.setEntity(se);
        
    

       
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
      
       
        

     
        HttpResponse httpResponse = httpclient.execute(httpPost);

    
        inputStream = httpResponse.getEntity().getContent();

    
        if(inputStream != null){
            result = convertInputStreamToString(inputStream);
            //JSONObject jObject = new JSONObject(result);
            
            //person.setrescode(jObject.getString("result"));
            
            //JSONArray list=  jObject.getJSONArray("vlist");
            
            //person.setliststore(jObject.getJSONArray("vlist"));
           // Log.d("ded1"," >>>>we here "+jObject.toString());
            System.out.println("Theresponse >>>>> "+ result);
            
        }
        else{
            result = "9999";
        }

    } catch (Exception e) {
        Log.d("InputStream", e.getLocalizedMessage());
    }
    
    Log.d("result", result);
 //   Log.i("ded"," >>>> "+result);
    // 11. return result
    return result;
    //return "we here";
}

	
	
	  private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	        String line = "";
	        String result = "";
	        while((line = bufferedReader.readLine()) != null)
	            result += line;
	 
	        inputStream.close();
	        return result;
	 
	    } 
	  
	  
 


}
