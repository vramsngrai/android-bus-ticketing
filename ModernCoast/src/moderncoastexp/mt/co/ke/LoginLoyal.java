package moderncoastexp.mt.co.ke;


import moderncoastexp.mt.co.ke.server.myconstants;

import com.andreabaccega.widget.FormEditText;

import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class LoginLoyal extends ActionBarActivity {
	
	   Button Loginbutton,skipbutton;
	   
	   FormEditText user_phone,user_loyalycard;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loyaltylogin);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
		
		
		
		
		SignupTextFields();
		LoginButton();
		 SkipButton();
		  
	}
	
	
	public void LoginButton() {
		 
		Loginbutton = (Button) findViewById(R.id.buttonsend);
 
		Loginbutton.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				FormEditText[] allFields    = {user_phone,user_loyalycard};
			    boolean allValid = true;
		        for (FormEditText field: allFields) {
		            allValid = field.testValidity() && allValid;
		        }

		        if (allValid) {	
		        	
		            if(new TestInternet().isOnline(v.getContext()))
				     {
		            	myconstants.userloyaltyNO=user_loyalycard.getText().toString();
		            	myconstants.userphoneNO=user_phone.getText().toString();
		            	Intent intent = new Intent(v.getContext(), busfrom.class);
		            	startActivity(intent);
		            	
		            	
				     }
				     else{
				    	 showToast("Error: No connection");
				     }
		        	
		        }
		        else {
		            // EditText are going to appear with an exclamation mark and an explicative message.
		        }
				
			     
			
				
			
				
 
			}
 
		});
 
	}
	
	
	public void SkipButton() {
		 
		skipbutton = (Button) findViewById(R.id.buttonredeem);
 
		skipbutton.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				myconstants.userloyaltyNO="";
		  
				Intent intent = new Intent(v.getContext(), busfrom.class);
            	startActivity(intent);
				
 
			}
 
		});
 
	}
	
	public void SignupTextFields() {
		
		//person = new GlobalData();        	
    	//datasource.getAllUserDet(person);
		user_phone = (FormEditText) findViewById(R.id.user_phone);
		user_loyalycard = (FormEditText) findViewById(R.id.user_pass);
    	
    	
		
		
		
		
	}
  
	
	  
	  public void showToast(String text){
		  Toast.makeText(this,text,
		           Toast.LENGTH_LONG).show();
	 }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	


}
