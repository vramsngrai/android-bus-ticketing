package moderncoastexp.mt.co.ke;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;


import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

public class busfrom extends ActionBarActivity {
	
	EditText mEdit;
	Spinner spinnerfrom,spinnerto;
	SimpleDateFormat sdf2=new SimpleDateFormat("dd-MM-yyyy");
	
	//SimpleDateFormat sdf1=new SimpleDateFormat("EEE , MMM d , yyyy");
	SimpleDateFormat sdf1=new SimpleDateFormat("EEE , d MMM , yyyy");
	Date datethis;
	 String AvailableSeats;
	 String TravelDateParam;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     
        setContentView(R.layout.activity_busfrom);
        

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
       
        spinnerfrom = (Spinner) findViewById(R.id.spinnerfrom);
        ArrayAdapter<CharSequence> adapterfrom = ArrayAdapter.createFromResource(
                this, R.array.busfrom_array, android.R.layout.simple_spinner_item);
        adapterfrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerfrom.setAdapter(adapterfrom);
        
        spinnerto = (Spinner) findViewById(R.id.spinnerto);
       ArrayAdapter<CharSequence> adapterto = ArrayAdapter.createFromResource(
               this, R.array.busto_array, android.R.layout.simple_spinner_item);
       adapterto.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       spinnerto.setAdapter(adapterto);
       
       mEdit = (EditText)findViewById(R.id.editTextDate);
       mEdit.setText(sdf1.format(new Date()));
       
       View myteximage = (View)findViewById(R.id.searchbusbtn);
		//mytextDiv.setText(labelId);

		myteximage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				
				
				if(new myconfigs().isOnline(v.getContext()))
				{
					 Intent intent = new Intent();
			         intent.setClass(v.getContext(), BusList.class);
			         intent.putExtra("thidate",mEdit.getText().toString() );
			         intent.putExtra("FromTown",spinnerfrom.getSelectedItem().toString());
			         intent.putExtra("ToTown",spinnerto.getSelectedItem().toString());
			         startActivity(intent);
					
				}
				
				else 
				{
					new myconfigs().showToastMe(v.getContext(),"oops! Device is disconnected");
				}
				
				
			}
		});
       
     //  
        
    }
    
  
    
    public void selectDate(View view) {
        DialogFragment newFragment = new SelectDateFragment();
        newFragment.show(getSupportFragmentManager(), "DatePicker");
    }
    public void populateSetDate(int year, int month, int day) {
    	
    	  try{
          	datethis = (Date) sdf2.parse(day+"-"+month+"-"+year);
          }
          catch (ParseException e) {
              System.out.println("Exception :" + e);
          }
    	
    	mEdit.setText(sdf1.format(datethis));
    }
    
    @SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    	@Override
    	public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar calendar = Calendar.getInstance();
			int yy = calendar.get(Calendar.YEAR);
			int mm = calendar.get(Calendar.MONTH);
			int dd = calendar.get(Calendar.DAY_OF_MONTH);
			return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    	}
    	
    	public void onDateSet(DatePicker view, int yy, int mm, int dd) {
    		populateSetDate(yy, mm+1, dd);
    	}
    }
    
	
    
    
    
}