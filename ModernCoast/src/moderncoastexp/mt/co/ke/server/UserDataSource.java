package moderncoastexp.mt.co.ke.server;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class UserDataSource {
	
	

  // Database fields
  private SQLiteDatabase database;
  private MySQLiteHelper dbHelper;
  private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
      MySQLiteHelper.COLUMN_fname , MySQLiteHelper.COLUMN_lname, MySQLiteHelper.COLUMN_email,
      MySQLiteHelper.COLUMN_phone , MySQLiteHelper.COLUMN_cardmd5 ,  MySQLiteHelper.COLUMN_cardmask, 
      MySQLiteHelper.COLUMN_pass};

  public UserDataSource(Context context) {
    dbHelper = new MySQLiteHelper(context);
  }

  public void open() throws SQLException {
    database = dbHelper.getWritableDatabase();
  }

  public void close() {
    dbHelper.close();
  }

  public long setDbuserdet(GlobalData person) {
	
    ContentValues values = new ContentValues();
    values.put(MySQLiteHelper.COLUMN_fname, person.getUserFname());
    values.put(MySQLiteHelper.COLUMN_lname, person.getUserLname());
    values.put(MySQLiteHelper.COLUMN_email, person.getUserEmail());
    values.put(MySQLiteHelper.COLUMN_phone, person.getUserphone());
    
    long insertId = database.insert(MySQLiteHelper.TABLE_userdet, null,
        values);
    
    /*Cursor cursor = database.query(MySQLiteHelper.TABLE_COMMENTS,
        allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
        null, null, null);
    cursor.moveToFirst();
    Comment newComment = cursorToComment(cursor);
    cursor.close();*/
    return insertId;
  }

  public void deleteUserdet(GlobalData person) {
    long id = person.getUserid();
    System.out.println("Comment deleted with id: " + id);
    database.delete(MySQLiteHelper.TABLE_userdet, MySQLiteHelper.COLUMN_ID
        + " = " + id, null);
  }

  public void getAllUserDet(GlobalData person) {
    

    Cursor cursor = database.query(MySQLiteHelper.TABLE_userdet,
        allColumns, null, null, null, null, null);

    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
    	person.setUserid(cursor.getInt(0));
    	person.setUserFname(cursor.getString(1));
        person.setUserLname(cursor.getString(2));
        person.setUserEmail(cursor.getString(3));
        person.setUserphone(cursor.getString(4));
      cursor.moveToNext();
    }
    // make sure to close the cursor
    cursor.close();
    
  }


} 