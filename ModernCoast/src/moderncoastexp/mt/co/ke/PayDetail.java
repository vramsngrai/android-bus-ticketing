package moderncoastexp.mt.co.ke;

import moderncoastexp.mt.co.ke.server.myconstants;

import org.json.JSONObject;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;


public class PayDetail extends ActionBarActivity{
	
	TextView payMessage,transtime,cardnumber,Messageerror,cardname,payamount;
	
	String paymesagestatus=myconstants.paydeclined;

	String JsonTsetail,Transdate;
	String Ramount,transid;
	 String busname; 
	 String busdeptime;
	 String busreptime;
	 String routecode;
	 String Traveldate;
	 String busId;
     String seatlist;
	 String bizprice;
	 String fclasprice ;
	 String vipsprice;
	 String SeatPrefix,seatNumber,Bfrom,Bto,seatType,CustomerClas,FinalPrice,mpesaref,
	 customerphone;
	 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trans_detail);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.tab_img));
		
		
		  JsonTsetail = getIntent().getSerializableExtra("JsonTsetail").toString();
	    
	       busdeptime = getIntent().getSerializableExtra("deptime").toString();
	       busreptime = getIntent().getSerializableExtra("reptime").toString();
	       busname = getIntent().getSerializableExtra("busname").toString();
	      // routecode = getIntent().getSerializableExtra("thidate").toString();
	       Traveldate = getIntent().getSerializableExtra("datetravel").toString();
	       busId = getIntent().getSerializableExtra("busid").toString();
	       seatlist = getIntent().getSerializableExtra("seatlist").toString();
	       seatNumber = getIntent().getSerializableExtra("seatno").toString();
	       bizprice = getIntent().getSerializableExtra("bizprice").toString();
	       fclasprice = getIntent().getSerializableExtra("fclassprice").toString();
	       vipsprice = getIntent().getSerializableExtra("vipprice").toString();
	       FinalPrice = getIntent().getSerializableExtra("finalprice").toString();
	       mpesaref = getIntent().getSerializableExtra("mpesaref").toString();
	       customerphone = getIntent().getSerializableExtra("phone").toString();
	       Bfrom = getIntent().getSerializableExtra("from").toString();
	       Bto = getIntent().getSerializableExtra("to").toString();
	       seatType = getIntent().getSerializableExtra("seatype").toString();
	       
	  Textfields();
		
	
	}
	//[{"stat":"okay","ticketno":"MO293","Busno":"MO293","Status":1}]
	public void Textfields()
	{
		try{
			
			JSONObject jObjectthis = new JSONObject(JsonTsetail);
		JSONObject jObject = new JSONObject(jObjectthis.getString("result"));
		
		if(jObject.getBoolean("ticketStatus")){
			paymesagestatus=myconstants.payapproved;
		}
		
		else{

			Messageerror = (TextView) findViewById(R.id.failuremessage);
			Messageerror.setText("Reason: "+jObject.getString("stat"));
		}
		
		payMessage = (TextView) findViewById(R.id.transtatus);
		payMessage.setText("Amount Paid:  "+jObject.getString("paid"));
		
		payamount = (TextView) findViewById(R.id.totalamount);
		payamount.setText(paymesagestatus);
		
		
		transtime = (TextView) findViewById(R.id.transtime);
		transtime.setText("Date: "+Traveldate);
		
		TextView reportime = (TextView) findViewById(R.id.transtimerort);
		reportime.setText("Reporting Time:   "+busreptime);
		
		TextView depttime = (TextView) findViewById(R.id.transdeptime);
		depttime.setText("Departure Time:   "+busdeptime);
		
		TextView tickenNo = (TextView) findViewById(R.id.tickrtno);
		tickenNo.setText("Ticket No:   "+jObject.getString("ticketno"));
		
		cardnumber = (TextView) findViewById(R.id.seatnumber);
		cardnumber.setText("Seat: "+seatNumber);
		
		
		
		
		
		} catch (Exception e) {
	        Log.d("InputStream", e.getLocalizedMessage());
	    }
		
	}
	

	
	

	  
	  public void showToast(String text){
		  Toast.makeText(this,text,
		           Toast.LENGTH_LONG).show();
	 }
	  @Override
		public boolean onOptionsItemSelected(MenuItem item) {
		   switch (item.getItemId()) {
		      case android.R.id.home:
		      
		         NavUtils.navigateUpTo(this,
		               new Intent(this, busfrom.class));
		         return true;
		   }
		   return super.onOptionsItemSelected(item);
		}
	  
}
